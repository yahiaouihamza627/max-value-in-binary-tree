<?php
declare(strict_types = 1);

include_once 'Node.php';

use Node\Node;

$root = new Node(10);
$node1 = new Node(5);
$node2 = new Node(7);
$node3 = new Node(13);
$root->setLeft($node1);
$root->setRight($node2);
$node2->setLeft($node3);
$result = findMaxNode($root);
echo 'Le node qui contient le maximum valeur : ' . $result;

function findMaxNode(Node $node) : int
{
    $leftMaxValue = $node->getLeft() != null ? findMaxNode($node->getLeft()) : PHP_INT_MIN;
    $rightMaxValue = $node->getRight() != null ? findMaxNode($node->getRight()) : PHP_INT_MIN;
    if($node->getValue() > $leftMaxValue && $node->getValue() > $rightMaxValue) return $node->getValue();
    if ($leftMaxValue > $rightMaxValue) return $leftMaxValue;
    return $rightMaxValue;
}