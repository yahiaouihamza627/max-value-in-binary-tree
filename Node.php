<?php
declare(strict_types=1);

namespace Node;

class Node {
    private ?Node $left;
    private ?Node $right;
    private int $value;

    public function __construct(int $value)
    {
        $this->value = $value;
        $this->left = null;
        $this->right = null;
    }

    public function getLeft() : ?Node
    {
        return $this->left;
    }

    public function setLeft($left) : void
    {
        $this->left = $left;
    }

    public function getRight() : ?Node
    {
        return $this->right;
    }

    public function setRight($right) : void
    {
        $this->right = $right;
    }

    public function getValue() : int
    {
        return $this->value;
    }

    public function setValue($value) : void
    {
        $this->value = $value;
    }


}